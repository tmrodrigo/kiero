<?php
require_once('php/database.php');

  $result = $db->query('select * from locals where local_state = "'.$_GET['provincia'].'" || local_district = "'.$_GET['provincia'].'" order by local_name asc');

$consulta = $result->fetchAll();

if(!empty($consulta)) {

  if (isset($_GET["encode"]) && $_GET["encode"] == "json") {
    foreach ($consulta as $local) {
      $buscar = array($local["local_address"] .", ". $local["local_district"] .", ". $local["local_state"]);
      $toJson[] = $buscar;
    }
    $j = json_encode($toJson);
    echo $j; die();
  }

  if (isset($_GET['provincia']) && isset($_GET['info'])) {
    foreach ($consulta as $local) {
      echo '
      <div class="row">
      <div class="item-result">
        <div class="head-result">
          <div class="item">
            <h3>'. strtoupper($local["local_name"]) . '</h3>
          </div>
          <div class="item">
            <h5>' . strtolower($local["local_address"]) .'</h5>
          </div>
          <div class="item">
            <h6>' . $local["local_phone"] . '</h6>
          </div>
          <div class="item">
            <p>' . strtolower($local["local_mail"]) . '</p>
          </div>
          <div class="item">
            <h5>' . $local["local_category"] . '</h5>
          </div>
        </div>
      </div>
      </div>';
    }
  }

  if(!empty($_GET['format']) && $_GET['format'] == 'json') {
      foreach ($consulta as $local) {
        $locacion = $local["district_name"];
        $provincia = $local["state_name"];
      }
  }
  if (!empty($_GET['provincia'] && !empty($_GET['district']))) {
    $r = $db->query("select local_district from locals where local_state = '" . $_GET['provincia'] . "' order by local_district asc ");

    $c = $r->fetchAll();

    foreach ($c as $value) {
      $arr[] = '<option value="'.strtolower($value['local_district'])  .'">' . strtolower($value['local_district']) . '</option>';
    }
    $dis = array_unique($arr);
    foreach ($dis as $d) {
      echo $d;
    }
  }
}
?>
