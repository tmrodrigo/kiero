
$(document).ready(function()
{

 $("#show_login").click(function(){
  showpopup();
 });
 $("#close_login").click(function(){
  hidepopup();
 });

 $(document).keyup(function(e){
   if(e.keyCode == 27) {
    hidepopup();
   }
 });

$( "#provincia" )
  .change(function () {
    var str = $( "#provincia" ).val();
    if(str != '') {
      $.get({
         url: 'mapa-result.php?provincia='+str+'&&district=1',
         success : function(response) {
           $('#district').empty();
            $('#district').append(response);
         }
      });

    }
  })
  .change();
  function showpopup()
  {
   $("#contact").fadeIn();
   $("#contact").css({"visibility":"visible","display":"block"});
   $("body").css({"overflow-y":"hidden"});
  };

  function hidepopup()
  {
   $("#contact").fadeOut();
   $("#contact").css({"visibility":"hidden","display":"none"});
   $("body").css({"overflow-y":"scroll"});
  };

  $(function(){
      var navMain = $(".navbar-collapse"); // avoid dependency on #id
      // "a:not([data-toggle])" - to avoid issues caused
      // when you have dropdown inside navbar
      navMain.on("click", "li:not([data-toggle])", null, function () {
          navMain.collapse('hide');
      });
  });

});
