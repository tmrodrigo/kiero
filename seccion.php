<?php require_once("php/database.php");

$nombreSeccion = $_GET["seccion"];
$tituloSeccion = $_GET["seccion"];

if ($nombreSeccion == "nautica")
{
  $tituloSeccion = "náutica";}

$query = $db->query('SELECT * from products where product_category = "'. $nombreSeccion .'" order by created_at desc');

$results = $query->fetchAll(PDO::FETCH_ASSOC);

require_once("php/database.php");
?>
<!DOCTYPE html>
<html lang="es">
<?php require_once("php/head.php");?>
  <body id="bodySeccion">
    <?php require_once("php/nav.php");?>
    <main>
      <div class="container-fluid">
        <div class="row">
          <section id="contact">
            <div class="container">
              <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                  <div class="formulario-contacto">
                    <button type="button" class="cerrar" id="close_login">x</button>
                    <h2>Contactanos</h2>
                    <form class="contacto" action="mail.php"  method="post">
                      <input type="text" name="nombre" value="" placeholder="Tu nombre" required>
                      <input type="email" name="email" value="" placeholder="Tu mail" required>
                      <input type="text" name="mensaje" value="" placeholder="Tu mensaje">
                      <div class="g-recaptcha" data-sitekey="6Le75DUUAAAAAHDFc0iBV6n5llupkXig7cL1YDHJ"></div>
                      <input type="submit" name="enviar" value="enviar">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section id="seccion" style="background-image: url(secciones/<?php echo $nombreSeccion;?>/bg-ppal.jpg)">
          </section>
          <section id="productos">
            <div class="container">
              <div class="texto-inicio">
                <div class="row">
                  <div class="col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
                    <h1><?= $tituloSeccion; ?></h1>
                    <h2>productos destacados</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="containerItem">
                <?php foreach ($results as $item) {  ?>
                  <div class="itemSeccion">
                    <article>
                      <img src="images/productos/<?=$nombreSeccion?>/<?=$item["product_img"]?>" alt="" class="img-producto">
                      <div class="text-producto">
                        <h4><?=$item["product_brand"]?></h4>
                        <h3><?=$item["product_name"]?></h3>
                        <h6>Código: <?=$item["product_art"]?></h6>
                        <p><?=$item["product_description"]?></p>
                      </div>
                    </article>
                  </div>
                <?php } ?>
              </div>
              <div class="row">
                <div class="col-sm-offset-4 col-sm-4 col-lg-offset-4 col-lg-4">
                  <a href="secciones/<?php echo $nombreSeccion; ?>/catalogo-<?=$nombreSeccion?>.pdf" target="_blank">ver catálogo completo</a>
                </div>
              </div>
            </div>
          </section>
          <?php require_once("php/newsletter.php"); ?>
        </div>
      </div>
    </main>
    <footer>
      <div class="container-fluid">
        <div class="row">
          <div class="direccion">
            <p>KIERO NORTE | Carlos Delcasse 2879, Ricardo Rojas  | +54 03327 443265<br><br>COMPRAS MAYORISTAS | +54 11 4220 6666</p>
          </div>
          <div class="copy">
            <p>© copyright 2017 - kiero SRL todos los derechos reservados - diseño y desarrollo <a href="http://loveinbrands.com" target="_blank">loveinbrands</a></p>
          </div>
        </div>
      </div>
    </footer>
    <?php require_once('php/footer-script.php');?>
</body>
</html>
