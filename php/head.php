<head>
  <?php $v = "3.1"; ?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <meta name="description" content="Representantes comerciales de las mejores marcas de pesca, outdoor y náutica en Argentina.">
  <link rel="icon" href="images/assets/favico.gif" type="image/gif" sizes="16x16">
  <title>Kiero SRL</title>
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,700" rel="stylesheet">
  <script src="https://use.fontawesome.com/f08f235f18.js"></script>

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>


  <link rel="stylesheet" href="css/estilos.css?<?=$v?>">
  <?php if (isset($_GET["seccion"])) {
    echo '<link rel="stylesheet" href="css/'. $_GET["seccion"] .'.css">';
  } ?>
  <script src="js/main.js?<?=$v?>"></script>

  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
