<div class="container">
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><img src="images/assets/logo.svg" alt="Kiero SRL - Pesca, Náutica y Outdoor"></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
          <li><a href="index.php#empresa">nosotros</a></li>
          <li><a href="seccion.php?seccion=pesca">pesca</a></li>
          <li><a href="seccion.php?seccion=outdoor">outdoor</a></li>
          <li><a href="seccion.php?seccion=nautica">náutica</a></li>
          <li><a href="mapa.php">locales</a></li>
          <li><button id="show_login">contacto</button></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="https://www.facebook.com/kierosrl" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.instagram.com/kierosrl/" target="_blank"><i class="fa fa-instagram"></i></a></li>
          <li><a href="https://www.youtube.com/channel/UCA-4hss3rugiTv2PzqCtpGA" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>
