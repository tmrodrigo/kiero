<style media="screen">

.promo-container {
  position: fixed;
  display: flex;
  justify-content: center;
  flex-flow: column;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 10000;
}

.promo-container .promo-wrapper {
  background-image: url('images/promo/coleman-vert.jpg');
  min-height: 650px;
  background-size: cover;
  background-position: center;
  display: flex;
  flex-flow: column;
  justify-content: center;
  padding: 8px;
}

.promo-container .promo-wrapper h2, .promo-container .promo-wrapper h3 {
  text-shadow: 2px 2px 4px black;
}

@media only screen and (min-width: 992px){
  .promo-container .promo-wrapper {
    background-image: url('images/promo/coleman-horz.jpg');
    justify-content: center;
    min-height: 450px;
  }

}

.promo-container .promo-wrapper a {
  margin: 24px auto;
  padding: 8px 24px;
  border: 2px solid white;
  background-color: white;
  color: black;
  border-radius: 0;
  font-weight: bold;
}

.promo-container .promo-wrapper img.coleman {
  width: 150px;
  position: absolute;
  right: 20px;
  bottom: 20px;
}

.promo-container .promo-wrapper img.kiero {
  width: 150px;
  position: absolute;
  left: 20px;
  top: 20px;
}

.promo-container .promo-wrapper a:hover {
  font-weight: bold;
  background-color: black;
  color: white;
}

.promo-container .promo-wrapper a.cerrar {
  margin: 24px auto;
  padding: 8px;
  border-radius: 50%;
  font-weight: bold;
  right: 16;
  top: -4px;
  line-height: 0.3;
}

.promo-container .promo-wrapper a.cerrar:hover {
  font-weight: bold;
  background-color: white;
  color: black;
}

</style>

<script type="text/javascript">
  $(document).ready(function(){

    function mostrar(){
      $('#promo').fadeIn('fast')
    }

    $('#promo').hide()

    setTimeout(mostrar, 2000)

    $('#cerrarPromo').on('click', function(e){
      e.preventDefault()
      $('#promo').hide()
    })
  })


</script>

<div class="promo-container" id="promo">
  <div class="row">
    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6">
      <div class="promo-wrapper">
        <div class="overlay"></div>
        <img src="images/promo/logo-coleman.png" alt="" class="coleman">
        <img src="images/promo/logo-kiero.png" alt="" class="kiero">
        <h2>Termo coleman</h2>
        <h3>Encontralo en tu comercio más cercano</h3>
        <a href="mapa.php" class="btn">Ver comercios</a>
        <a href="" class="cerrar" id="cerrarPromo">x</a>
      </div>
    </div>
  </div>
</div>
