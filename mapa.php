<?php
  require_once("php/database.php");
  $locales = [];
  $pNombre = "";

  $query = $db->query('select local_state, local_district from locals order by local_state asc');

  $locales = $query->fetchAll(PDO::FETCH_ASSOC);

  foreach ($locales as $value) {
    $arr[] = '<option value="'.strtolower($value['local_state'])  .'">' . strtolower($value['local_state']) . '</option>';
  }
  $locaciones = array_unique($arr);

  if ($_GET) {

    if (isset($_GET["provincia"])) {

      $provincia = $_GET["provincia"];
      $e = substr($provincia, 0, 5);
      $query = $db->query('select * from locals where local_district Like "'.$e.'%" || local_state Like "'.$e.'%"');

      $locales = $query->fetchAll(PDO::FETCH_ASSOC);

      $q = $db->query('select * from locals where local_district Like "'.$e.'%" || local_state Like "'.$e.'%" ');
      $cant = $q->fetchAll(PDO::FETCH_ASSOC);

      $c = count($cant);
      if (empty($results)) {
        $noHay = "No se encontraron productos";
      }
    }

    if(isset($_GET["seccion"]) && isset($_GET["provincia"])){
      $provincia = $_GET["provincia"];
      $e = substr($provincia, 0, 5);
      $query = $db->query('select * from locals where (local_district Like "'.$e.'%" || local_state Like "'.$e.'%") && '.$_GET["seccion"].' Like "'.$_GET["seccion"].'%"');

      $locales = $query->fetchAll(PDO::FETCH_ASSOC);
    }

    foreach ($locales as $local) {
      $pNombre = $local["local_district"];
      $direccion = $local["local_address"];
      $localidad = $local["local_district"];
      $provincia = $local["local_state"];
      $buscar = array($direccion .", ". $localidad .", ". $provincia);
      $toJson[] = $buscar;
    }
  }

?>

<!DOCTYPE html>
<html lang="es">
<?php require_once("php/head.php"); ?>
  <body>
    <?php require_once("php/nav.php"); ?>
    <main id="mapa">
      <div class="container-fluid">
        <div class="row">
          <section id="contact">
            <div class="container">
              <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                  <div class="formulario-contacto">
                    <button type="button" class="cerrar" id="close_login">x</button>
                    <h2>Contactanos</h2>
                    <form class="contacto" action="mail.php" method="post">
                      <input type="text" name="nombre" value="" placeholder="Tu nombre" required>
                      <input type="email" name="email" value="" placeholder="Tu mail" required>
                      <input type="text" name="mensaje" value="" placeholder="Tu mensaje">
                      <input type="submit" name="enviar" value="enviar">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <section class="buscar">
          <div class="container">
            <div class="row">
              <div class="col-sm-4">
                <h1>Buscá tu local más cercano: </h1>
              </div>
              <div class="col-sm-8">
                <div class="buscador-inicio">
                  <form class="buscador" action="mapa.php" method="get" autocomplete="off"  id="submit">
                    <select class="" name="provincia" id="provincia">
                      <?php foreach ($locaciones as $locacion) { ?>
                        <?=$locacion?>
                      <?php } ?>
                    </select>
                    <select class="" name="locacion" id="district"></select>
                    <button type="button" onclick="clickMap()"><i class="fa fa-search"></i></button>
                  </form>
                  <script type="text/javascript">
                    $(window).on('load', function(){
                      $('#submit button').click()
                    })
                  </script>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="container">
            <div id="map"></div>
          </div>
        </section>
        <section class="locales">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <h2>Locales en:<br /><span id="q"></span> </h2>
              </div>
              <div class="col-sm-6">
                <div class="paginador">
                  <ul>

                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div id="info">

                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="container">
          <?php require_once("php/newsletter.php"); ?>
        </div>
      </div>
    </main>
    <footer>
      <div class="container-fluid">
        <div class="row">
          <div class="direccion">
            <p>KIERO NORTE | Carlos Delcasse 2879, Ricardo Rojas  | +54 03327 443265<br><br>COMPRAS MAYORISTAS | +54 11 4220 6666</p>
          </div>
          <div class="copy">
            <p>© copyright 2017 - kiero SRL todos los derechos reservados - diseño y desarrollo <a href="http://loveinbrands.com" target="_blank">loveinbrands</a></p>
          </div>
        </div>
      </div>
    </footer>
    <?php require_once('php/footer-script.php');?>

    <script type="text/javascript">
    var MyMap;
    var dir;
    var markers = [];
    var latlng;
    var addresses;

    function initMap() {

          var MapOptions = {
            center: new google.maps.LatLng(-34.603629, -58.381635),
            zoom: 5,
          }
          MyMap = new google.maps.Map(document.getElementById('map'), MapOptions);

          var url_string = window.location.href ;
          var url = new URL(url_string);
          var c = url.searchParams.get("provincia");
          var d = url.searchParams.get("locacion");

          var optP = "<option value='"+c+"'>"+c+"</option>"
          var optL = "<option value='"+d+"'>"+d+"</option>"
          var t = d + ', ' + c;

          if (d) {
            var dir = 'mapa-result.php?provincia='+d+'&&encode=json';

            var url = 'mapa-result.php?provincia='+d+'&&info=1';

              getData(dir);
              getInfo(url);
              $('#provincia').append(optP);
              $('#q').append(t);
              $('#district').append(optL);
              scrollTo('submit');
          }
      }

      $(document).keyup(function(e){
        if(e.keyCode == 13) {
         clickMap();
        }
      });

    function clickMap(){
      var p = $('#provincia').val();
      var q = $('#district').val();
      var t = q + ', ' + p ;

      var dir = 'mapa-result.php?provincia='+q+'&&encode=json';

      var url = 'mapa-result.php?provincia='+q+'&&info=1';

        getData(dir);
        getInfo(url);
        $('#input').val("");
        $('#q').empty();
        $('#q').append(t);
        $('#result').empty();
        $('#result').css('overflow-y', 'hidden');
        scrollTo('submit');
    }

    function scrollTo(hash) {
    location.hash = "#" + hash;
    }
    function getData(q){
      $.getJSON(q, function(data) {
         var arr = data;
         const addresses = arr.reduce((acc, val) => {
           return acc.concat(val)
         }, []);
         loadMarkers(addresses);
         setCenter(addresses[0]);
         // debugger
       });
    }
    function loadMarkers(addresses) {
      //borramos marcadores existentes
      if (markers.length) {
        deleteMarkers();
      }
      createMarkers(addresses)
    }

    function deleteMarkers() {
      markers.forEach(function(marker){
        marker.setMap(null)
      })
    }

    function createMarkers(addresses){
      for (var x = 0; x < addresses.length; x++) {
         $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&key=AIzaSyDqHvkmGFxFIfNhao8ZiO-wggcdLnYxdQk', null, function (data) {
          var p = data.results[0].geometry.location;
          var latlng = {lat: p.lat, lng: p.lng};
          markers.push(new google.maps.Marker({
            position: latlng,
            map: MyMap
          }))
         });
       }
    }

    function setCenter(address){
      $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyDqHvkmGFxFIfNhao8ZiO-wggcdLnYxdQk', null, function (data) {
        var p = data.results[0].geometry.location;
        lat = p.lat;
        lng = p.lng;
        moveToLocation(lat, lng);
      });
    }

    function moveToLocation(lat, lng){
      var center = new google.maps.LatLng(lat, lng);
      MyMap.panTo(center);
      MyMap.setZoom(15);
    }

    $("#submit").submit(function(e){
    return false;
    });

    function getInfo(u){
      $.get({
         url: u,
         success : function(response) {
           $('#info').empty();
           $('#info').append(response);
         }
      });
    }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqHvkmGFxFIfNhao8ZiO-wggcdLnYxdQk&callback=initMap">
    </script>
  </body>
</html>
