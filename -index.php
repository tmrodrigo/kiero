<?php
$mensajeEnviado = "";
if (isset($_GET["m"]) && $_GET["m"] == 1){
  $mensajeEnviado = '<div class="exito" id="alerta">
    <h4>Su mensaje ha sido enviado, en breve le contestaremos</h4>
  </div>';
}

if (isset($_GET["m"]) && $_GET["m"] == 0){
  $mensajeEnviado = '<div class="exito" id="alerta">
    <h4>Ocrurrió un error en el servidor, por favor intente más tarde</h4>
  </div>';
}
require_once("php/database.php");
$locales = [];
$pNombre = "";

$query = $db->query('select local_state, local_district from locals order by local_state asc');

$locales = $query->fetchAll(PDO::FETCH_ASSOC);

foreach ($locales as $value) {
  $arr[] = '<option value="'.strtolower($value['local_state'])  .'">' . strtolower($value['local_state']) . '</option>';
}
$locaciones = array_unique($arr);
?>
<!DOCTYPE html>
<html lang="es">
<?php require_once("php/head.php"); ?>
  <body>
    <?php require_once("php/nav.php"); ?>
    <main>
      <div class="container-fluid">
        <div class="row">
          <section id="contact">
            <div class="container">
              <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                  <div class="formulario-contacto">
                    <button type="button" class="cerrar" id="close_login">x</button>
                    <h2>Contactanos</h2>
                    <form class="contacto" action="mail.php" method="post">
                      <input type="text" name="nombre" value="" placeholder="Tu nombre" required>
                      <input type="email" name="email" value="" placeholder="Tu mail" required>
                      <input type="text" name="mensaje" value="" placeholder="Tu mensaje">
                      <div class="g-recaptcha" data-sitekey="6Le75DUUAAAAAHDFc0iBV6n5llupkXig7cL1YDHJ"></div>
                      <input type="submit" name="enviar" value="enviar">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section id="home">
            <div class="container-fluid">
              <div class="texto-inicio">
                <div class="row">
                  <div class="col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
                    <h1>Kiero <span>SRL</span></h1>
                    <h4>las mejores marcas en argentina</h4>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                  <?php echo $mensajeEnviado ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-offset-2 col-sm-8 col-lg-offset-4 col-lg-4">
                  <div class="buscador-inicio">
                    <form class="buscador" action="mapa.php" method="get" autocomplete="off"  id="submit">
                      <select class="" name="provincia" id="provincia">
                        <?php foreach ($locaciones as $locacion) { ?>
                          <?=$locacion?>
                        <?php } ?>
                      </select>
                      <select class="" name="locacion" id="district"></select>
                      <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div id="carousel-generic" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <div class="carousel-inner">
                      <div class="item active">
                        <img src="images/assets/marcas/marcas-01.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-02.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-03.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-04.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-05.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-06.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-07.svg" alt="">
                      </div>
                      <div class="item">
                        <img src="images/assets/marcas/marcas-08.svg" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section id="secciones">
            <div class="container-fluid">
              <div class="row">
                <article class="nautica" id="nautica">
                    <div class="col-sm-6">
                      <div class="text-item">
                        <h4>kiero</h4>
                        <h2>náutica</h2>
                        <a href="seccion.php?seccion=nautica">ver más</a>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="img-item">
                        <img src="images/home/item-nautica.jpg" alt="Kiero SRL, las mejores marcas de náutica">
                      </div>
                    </div>
                </article>
              </div>
              <div class="row">
                <article class="pesca" id="pesca">
                  <div class="col-sm-6 fl-right">
                    <div class="text-item">
                      <h4>kiero</h4>
                      <h2>pesca</h2>
                      <a href="seccion.php?seccion=pesca">ver más</a>
                    </div>
                  </div>
                  <div class="col-sm-6 fl-left">
                    <div class="img-item">
                      <img src="images/home/item-pesca.jpg" alt="Kiero SRL, las mejores marcas de pesca">
                    </div>
                  </div>
                </article>
              </div>
              <div class="row">
                <article class="outdoor" id="outdoor">
                  <div class="col-sm-6">
                    <div class="text-item">
                      <h4>kiero</h4>
                      <h2>outdoor</h2>
                      <a href="seccion.php?seccion=outdoor">ver más</a>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="img-item">
                      <img src="images/home/item-outdoor.jpg" alt="Kiero SRL, las mejores marcas de outdoor">
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </section>
          <section id="empresa">
            <div class="text-empresa">
              <div class="container">
                <div class="row">
                  <div class="col-sm-6">
                    <h2>Kiero <span>SRL</span></h2>
                  </div>
                  <div class="col-sm-6">
                    <p>Desde 1968, somos representantes comerciales de las mejores marcas de pesca, outdoor y náutica en Argentina. Con una vasta experiencia y vendedores especializados, abastecemos a todo el país desde nuestro depósito de 2.500m2 situado en Villa Dominico, partido de avellaneda, a tan solo 20 minutos del obelisco.<br><br>Contamos con un amplio stock, personal capacitado y un sistema de gestión que nos permiten una rápida preparación y control de los pedidos. Con flota propia, vistamos diariamente los expresos que llevan los pedidos a cada rincón del país.<br><br>Nuestras marcas: Abu Garcia, Berkley, Hyperlite, Coleman, Nite Ize, National Geographic, Mitchell, North Carolina, SportStuff, Minnkota, Aquafloat, Sasame, Carolina Skiff, Humminbird, Power Tec, Wow, Scepter, Sterns, Spider Wire, Sevlor, Penn, Flambeau, Moeller, Sierra, LD, Skis, Chums, FenWick, Attwood, Lalizas, Shout, Cannon, Nuova Rade, Shakespeare.
                  </div>
                </div>
              </div>
            </div>
          </section>
          <?php require_once("php/newsletter.php"); ?>
        </div>
      </div>
    </main>
    <footer>
      <div class="container-fluid">
        <div class="row">
          <div class="direccion">
            <p>KIERO NORTE | Carlos Delcasse 2879, Ricardo Rojas  | +54 03327 443265<br><br>COMPRAS MAYORISTAS | +54 11 4220 6666</p>
          </div>
          <div class="copy">
            <p>© copyright 2017 - kiero SRL todos los derechos reservados - diseño y desarrollo <a href="http://loveinbrands.com" target="_blank">loveinbrands</a></p>
          </div>
        </div>
      </div>
    </footer>
    <?php require_once('php/footer-script.php');?>
</body>
</html>
